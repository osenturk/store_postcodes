""" postcodes.io services are implemented here
        * Bulk Postcode Lookup
        POST - https://api.postcodes.io/postcodes

        def get_bulk_latlong_by_postcodes(store_dict, request_limit):

        * Postcode Lookup
        GET - https://api.postcodes.io/postcodes/:postcode

        def get_latlong_by_postcode(postcode):

        * Nearest Postcode by given Postcode
        GET - https://api.postcodes.io/postcodes/:postcode/nearest

        def get_nearest_postcodes_by_radius_and_postcode(radius, postcode, radius_limit):

         * Nearest Stores by given Postcode

        def get_nearest_stores_by_radius_and_postcode(radius, postcode, radius_limit, store_dict):

    some tips to remember by http://www.compassdude.com/latitude-longitude.php
    Latitude is always given before longitude
    Degrees West and South are sometimes referred to as negative degrees

"""

__author__ = "Ozan Senturk"

import logging
import urllib.parse
import requests

from requests.exceptions import HTTPError

# custom packages for the project

from services.model.postcode_response import PostcodeResponse


logger = logging.getLogger(__name__)


def get_bulk_latlong_by_postcodes(store_dict, request_limit):
    """
    gets longitude and latitude by a dictionary of stores and
    a request limit. If the number of store in the dictionary is
    bigger than the request limit, the results come as batch of the
    request limit. For example, if the size of the dictionary is 1000
    and the request_limit is 100, then 10 consecutive requests are
    submitted to get the latitudes and the longitudes of each store in
    the dictionary implicitly. Also, there is a Bulk Postcode Lookup
    constraint of 100 by postcode.io

    :param store_dict: caches the stores by postcode
    :param request_limit: the batch limit for each bulk request
    :return:
        store dictionary where the key is the postcode and
        the value is store object
    """

    post_codes = list(store_dict)  # turning a dictionary into a list
    # results in a list of all the keys:

    total_postcodes = len(post_codes)
    logger.info("the number of postcodes is %d", total_postcodes)

    request_data = dict()

    for index in range(0, total_postcodes, request_limit):

        request_data["postcodes"] = post_codes[index: (index + request_limit)]

        try:
            response = requests.post(
                "https://api.postcodes.io/postcodes", data=request_data
            )

            r_json = response.json()  # returns json format

            # If the response was successful, no Exception will be raised
            response.raise_for_status()

        except HTTPError as http_err:
            logger.error("HTTP error occurred: %s", http_err)
        except Exception as err:
            logger.error("Other error occurred: %s", err)
        else:
            logger.info("Request to get bulk longs and lats is successful ")

        if (
            r_json["status"] == PostcodeResponse.HTTP_SUCCESS
        ):  # if the request is success
            response_list = r_json["result"]

            for response in response_list:  # GU14 7QL


                    # find the store by postcode in the dictionary
                    # ,update both longitude and latitude, and store back
                    # to the dictionary

                postcode = response["query"]
                logger.debug("The postcode is %s", postcode)

                if response["result"] is not None:

                    store = store_dict[postcode]
                    store.long = float(response["result"]["longitude"])
                    store.lat = float(response["result"]["latitude"])
                    store_dict[postcode] = store
                else:
                    logger.warning(
                        "postcode %s has no longitude and latitude", postcode
                    )

        else:
            logger.error(
                "error code: %d error message: %s", r_json["status"],
                r_json["error"]
            )

    return store_dict


def get_latlong_by_postcode(postcode):
    """
    postcode look up gets the latitude and longitude for each
    postcode as a tuple. Tuples are immutable which favors
    security that immutable objects cannot be changed and
    concurrency that immutability make the objects automatically
    thread-safe.

    :param postcode:
    :return:
        tuple of latitude and longitude as (latitude, longitude)
    """
    try:
        encoded_postcode = urllib.parse.quote(postcode)

        response = requests.get(
            "https://api.postcodes.io/postcodes/" + encoded_postcode
        )

        r_json = response.json()  # returns json format

        # If the response was successful, no Exception will be raised
        response.raise_for_status()

    except HTTPError as http_err:
        logger.error("HTTP error occurred: %s", http_err)
    except Exception as err:
        logger.error("Other error occurred: %s", err)
    else:
        logger.info("Request to get bulk longs and lats is successful ")

        if (
            r_json["status"] == PostcodeResponse.HTTP_SUCCESS
        ):  # if the request is success

            if r_json["result"] is not None:

                long = r_json["result"]["longitude"]
                lat = r_json["result"]["latitude"]

            else:
                logger.warning("postcode %s has no longitude and latitude",
                               postcode)

        else:
            logger.error(
                "error code: %d error message: %s", r_json["status"],
                r_json["error"]
            )

    return (lat, long)  # tuple for security and thread safe


def get_nearest_postcodes_by_radius_and_postcode(radius, postcode, radius_limit):
    """
        Gets the nearest postcodes by given radius and postcodes.
        Default is to 100m and the max is 2000m for the radius and
         the radius limit is 10 by default and the max of it is 100.

    :param radius:
    :param postcode:
    :param radius_limit:
    :return: postcode list
    """

    logger.info("the postcode is %s and the radius is %s", postcode, radius)

    postcode_list = []

    request_data = {"radius": radius, "limit": radius_limit}

    try:

        response = requests.get(
            "https://api.postcodes.io/postcodes/" + postcode + "/nearest?",
            params=request_data,
        )

        r_json = response.json()  # returns json format

        # If the response was successful, no Exception will be raised
        response.raise_for_status()

    except HTTPError as http_err:

        logger.error("HTTP error occurred: %s", http_err)

    except Exception as err:

        logger.error("Other error occurred: %s", err)

    else:
        logger.info(
            "Request to get postcode for a given radius "
            "and a postcode is successful "
        )

        if (
            r_json["status"] == PostcodeResponse.HTTP_SUCCESS
        ):  # if the request is success

            response_list = r_json["result"]
            if response_list is None:
                logger.warning(
                    "no postcode found around given radius %s and "
                    "postcode %s",
                    postcode,
                    radius,
                )
            else:
                for response in response_list:
                    postcode = response["postcode"]
                    logger.debug("The postcode is %s", postcode)

                    postcode_list.append(postcode)

        else:
            logger.error(
                "error code: %d error message: %s", r_json["status"],
                r_json["error"]
            )

    logger.debug(tuple(postcode_list))
    return tuple(postcode_list)  # tuple for security due to immutable


def get_nearest_stores_by_radius_and_postcode(radius, postcode, radius_limit, store_dict):
    """
        Gets nearest stores of list. First, get the nearest stores by
        radius and postcode. Then, filters the postcodes which belong
        to the pet stores inside the found postcodes. Thereafter, get
        the latitude and longitude for each filtered postcode. Finally,
        it sorts the stores based on the latitude to perform the sort
        from North to South.

    :param radius:
    :param postcode:
    :param radius_limit:
    :param store_dict:
    :return: list of stores
    """

    postcode_list = get_nearest_postcodes_by_radius_and_postcode(
        radius, postcode, radius_limit
    )

    store_postcodes = list(store_dict)  # list keeps the key of the dictionary


    found_postcodes = list(
        filter(lambda store: store in store_postcodes, postcode_list)
    )

    found_stores = [store_dict[postcode] for postcode in found_postcodes]

    for store in found_stores:
        lat_long = get_latlong_by_postcode(store.postcode)
        store.lat = lat_long[0]
        store.long = lat_long[1]

    found_stores.sort(key=lambda x: x.lat,
                              reverse=True)  # sort by lat

    return found_stores
