"""
    Data related services are located in this module
"""

__author__ = "Ozan Senturk"

import json
import logging
import yaml


# custom packages for the project
from services.model.store import Store

logger = logging.getLogger(__name__)


def load_config():
    """
    initializing the configuration

    :return:
        the configuration object
    """

    config_path = "config.yaml"

    logger.info("the location of the configuration file is %s", config_path)

    with open(config_path, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    return cfg


def import_stores(file_path):
    """
    load the json file into the memory

    :param file_path:
    :return:
        store dictionary where the key is the postcode and
        the value is store object
    """

    store_dict = dict()

    with open(file_path) as file:
        json_data = json.load(file)

    for json_store in json_data:

        one_store = Store(json_store["name"], json_store["postcode"])
        logger.debug("the store data is %s", one_store)
        store_dict[one_store.postcode] = one_store

    return store_dict
