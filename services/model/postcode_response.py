"""
This IntEnum object represents the http response codes
by postcode.io errors in more readable format
"""
from enum import IntEnum


class PostcodeResponse(IntEnum):
    """
        The http response codes are presented
        in constant variables
    """

    HTTP_SUCCESS = 200
    HTTP_BAD_REQUEST = 400
    HTTP_NOT_FOUND = 404
    HTTP_SERVER_ERROR = 500
