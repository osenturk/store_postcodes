"""
Store object is the wrapper class for the store data in JSON format.
"""
__author__ = "Ozan Senturk"


class Store:
    """
        Store object contains one constructor to instantiate the class
        and one __repr__ method to print the state of the class
    """

    # default latitude and longitude are None
    def __init__(self, name, postcode, lat=None, long=None):
        """
        initialize the Store object

        :param name:
        :param postcode:
        :param long:
        :param lat:
        """
        self.name = name
        self.postcode = postcode
        self.long = long
        self.lat = lat

    def __repr__(self):
        """
        meaningful representation of the Store object to print

        :return:
        """
        return "<Store( name='%s', postcode='%s, lat='%s, long='%s ')>" % (
            self.name,
            self.postcode,
            self.lat,
            self.long,
        )
