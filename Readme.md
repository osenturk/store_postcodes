 
#Environment setup

install the packages used in the project
```python
pip install -r requirements.txt
```

# General Instructions
1. Run the intro.py 
```python
 python intro.py
```

2. click the 'http://0.0.0.0:4000/' for the index page

3. the browser will be popped up and choose one of the service.
    1. List stores
    2. Get latitude and longtitude for each store
    
    there are 'home' link at the bottom of each page to return
    to the index page


This program takes JSON format 'Pet Stores' data and communicates 
with postcodes.io REST APIs to get further information about
latitude and longitude information of each store and nearer stores
by given radius and postcode.

The program composed of 5 directories, 2 configuration files,
one readme file, and one main program at the root directory.
Directories are 'data', 'services', 'static', 'templates', and
'tests' whilst configuration files are 'config.yaml' and 
'logging.conf'. The main program is the 'intro.py' where results
are rendered.

Data directory contains 3 test data files. The 'stores.json' keeps
the name and postcode of each store. The 'nearer_stores.json'
is to test the nearer stores for the 4th requirement. The
'response_long_lat.json' is the sample response from 'Bulk Postcode
Lookup' from postcode.io to test the 3rd requirement.

Services directory contains one 'model' directory where the 'Store'
object and 'PostcodeResponse' enum are kept. Store object wraps 
the Store JSON data and PostcodeResponse makes code easier to read
while working with numerical http return codes. Service directory
also has two python modules which are 'data_services.py' and 
'postcode_services.py'. The 'data_services.py' is responsible for
file read operations whilst the 'postcode_services.py' is responsible
for the postcode related services including getting latitude and 
longitude for a single postcode, bulk postcode lookups, and nearest
postcodes and stores by given postcodes and radius.

Static directory where the files such as photos and css are kept.
Jinja2 template requires a folder named 'static' to be able to access
files from the html.

Templates directory is required for html files to render the information
provided by the services.

Tests directory where the all the tests are performed. Current test
coverage is 86% and can be seen with the command below

```python
pytest --cov-report term-missing --cov=services tests/
```

Additionally, logging is used to easily maintain the debugging 
and current loglevel is set to DEBUG. In production it is better 
to set it to the WARNING or ERROR based on the frequency of the logs.

error = 50
debug = 10

Also. config.yaml is used to customize some variables without changing 
the code 

#### Tell us what test you completed 
backend

#### Tell us what you'd have changed if you'd have had more time?

* Paging for the rendering of the postcode results
* Adding basic authentication for the Flask REST services 
 
#### What bits did you find the toughest? What bit are you most proud of? In both cases, why?

The toughest part is to find test data to validate nearest 
post codes as the radius of 2000m did not work with current test 
data 'stores.json'. Therefore, I created a new data set called 
'nearer_stores.json'.

I liked the part where I added 'buffering' for the 3rd requirement 
which asks for using postcodes to get the latitude and longitude for 
each post code. Given that current data set has 95 entries, it is 
less than the postcode.io request limit of 100 for the bulk postcode 
lookup service. Thus, it works well with the current data set. But 
if there were 200 entries, there would be a need of submitting requests
in batches where each batch is equal to 100 or less. That's why
I added buffering with the configuration parameter of 
'bulk_postcode_limit' in the config.yaml file. Now, even there were
1000 entries, the buffering at 'get_bulk_latlong_by_postcodes' 
service would get latitude and longitude values by batches of 
bulk_postcode_limit. For example, if the limit is 100, the service
will be called 10 times to get 1000 result implicitly.
 

#### What's one thing we could do to improve this test?
There might be two more data sets which are beyond the postcode.io
 limit of 100. For example, one data set may contain 200 entries and
 another data set may contain 10,000 entries. Having 200 entries data
 set will force to find a solution to send request in batches. Also,
 having 10,000 entries will force to use efficient data structures 
 such as 'deque' instead of 'list'. Deques have O(1) speed for
 appendleft() and popleft() whilst lists have O(n) performance for
 insert(0, value) and pop(0).

#Test setup

Each service has al least two test cases. One of them is to test the 
API provided by 'postcode.io' and one of them is to test the wrapper 
service implemented by the project. **Project calls the wrapper 
version of the services to avoid boiler plate code**.


def test_get_postcodes_by_radius_and_postcode_api(get_config):

def test_get_postcodes_by_radius_and_postcode(get_config):
