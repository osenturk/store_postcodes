"""
    testing postcode.io services

    outcode
        Postcode Area
        Postcode District

    incode
        Postcode Sector
        Unit Postcode

        W6: Outcode 9SR: Incode

    https://www.getthedata.com/postcode
"""

__author__ = "Ozan Senturk"

import json
import urllib.parse
import requests

# custom packages

from services.postcode_services import get_bulk_latlong_by_postcodes
from services.data_services import import_stores
from services.model.postcode_response import PostcodeResponse
from services.postcode_services import get_latlong_by_postcode
from services.postcode_services \
    import get_nearest_postcodes_by_radius_and_postcode
from services.postcode_services \
    import get_nearest_stores_by_radius_and_postcode

WIDE_SEARCH_LIMIT = 10


def test_get_bulk_latlong_by_postcodes_api():
    """
        tests the postcode.io bulk postcode lookup api.

    :return:
    """

    test = {"postcodes": ["AL1 2RJ", "AL9 5JP"]}

    response = requests.post("https://api.postcodes.io/postcodes", data=test)

    assert response.status_code == 200

    r_json = response.json()  # returns json format

    assert r_json["status"] == 200

    postcode_list = r_json["result"]

    assert len(postcode_list) == 2


def test_parse_get_bulk_latlong():
    """
        tests to parse the response of bulk postcode lookup

    :return:
    """

    with open("data/response_long_lat.json") as file:
        json_data = json.load(file)

        response_list = json_data["result"]

        assert response_list[0]["result"]["longitude"] == -1.069849
        assert response_list[0]["result"]["latitude"] == 51.656146


def test_get_bulk_latlong_by_postcodes(get_config):
    """
        test to get bulk postcode lookup of the postcode services

    :param get_config:
    :return:
    """

    file_path = get_config["store"]["file_path"]

    store_dict = import_stores(file_path)

    assert len(store_dict) == 95

    # limit per request for postcode.id
    request_limit = get_config["postcode_io"]["bulk_postcode_limit"]

    store_dict = get_bulk_latlong_by_postcodes(store_dict, request_limit)

    assert store_dict["AL1 2RJ"] is not None
    assert len(store_dict) == 95


def test_postcode_response():
    """
    tests whether IntEnum type is comparable with http status codes
    :return:
    """
    assert PostcodeResponse.HTTP_SUCCESS == 200
    assert PostcodeResponse.HTTP_BAD_REQUEST == 400


def test_get_longlat_by_postcode_api():
    """
        tests the postcode.io single postcode lookup api
    :return:
    """

    postcode = "W6 9SR"
    encoded_postcode = urllib.parse.quote(postcode)

    response = requests.get("https://api.postcodes.io/postcodes/" + encoded_postcode)

    assert response.status_code == PostcodeResponse.HTTP_SUCCESS

    r_json = response.json()  # returns json format

    assert r_json["status"] == PostcodeResponse.HTTP_SUCCESS

    long = r_json["result"]["longitude"]
    lat = r_json["result"]["latitude"]

    assert long == -0.221455
    assert lat == 51.483156


def test_get_latlong_by_postcode():
    """
        tests to get the latitude and longitude of postcode
        for the postcode services

    :return:
    """

    postcode = "W6 9SR"

    result = get_latlong_by_postcode(postcode)

    assert result[0] == 51.483156  # latitude
    assert result[1] == -0.221455  # longitude


def test_get_nearest_postcodes_by_radius_and_limit_api(get_config):
    """
        tests to get nearest postcodes by given radius and postcode for
        the postcode.io api

    :param get_config:
    :return:
    """

    cantenbury = "CT1 3TQ"
    radius = get_config["postcode_io"]["radius"]
    radius_limit = get_config["postcode_io"]["radius_limit"]

    test_data = {"radius": radius, "limit": radius_limit}

    response = requests.get(
        "https://api.postcodes.io/postcodes/" + cantenbury + "/nearest?",
        params=test_data,
    )

    assert response.status_code == PostcodeResponse.HTTP_SUCCESS

    r_json = response.json()  # returns json format

    assert r_json["status"] == PostcodeResponse.HTTP_SUCCESS

    post_list = r_json["result"]

    assert len(post_list) == radius_limit


def test_get_nearest_postcodes_by_radius_and_limit(get_config):
    """
        tests to get nearest postcodes by given radius and postcode for
        the postcode services

    :param get_config:
    :return:
    """

    cantenbury_sturry_road = "CT1 1DX"
    radius = get_config["postcode_io"]["radius"]
    radius_limit = get_config["postcode_io"]["radius_limit"]

    nearer_postcodes = ["CT1 1DS", "CT1 1DT", "CT1 1EU", "CT1 1EX", "CT1 1EE"]
    eastbourne = "BN23 6QD"  # too much South

    postcode_list = get_nearest_postcodes_by_radius_and_postcode(
        radius, cantenbury_sturry_road, radius_limit
    )

    assert len(postcode_list) > 1

    found_postcodes = list(
        filter(lambda store: store in nearer_postcodes, postcode_list)
    )

    assert nearer_postcodes[1] in found_postcodes
    assert nearer_postcodes[2] in found_postcodes
    assert eastbourne not in found_postcodes


def test_get_nearest_stores_by_radius_and_postcode(get_config):
    """
        tests to get nearest stores by given radius and postcode for
        the postcode services

    :param get_config:
    :return:
    """

    cantenbury_sturry_road = "CT1 1DX"
    radius = get_config["postcode_io"]["radius"]
    radius_limit = get_config["postcode_io"]["radius_limit"]

    file_path = "data/nearer_stores.json"

    store_dict = import_stores(file_path)

    found_stores = \
        get_nearest_stores_by_radius_and_postcode(radius,
                                                  cantenbury_sturry_road, radius_limit, store_dict)

    assert found_stores[0].lat > found_stores[1].lat # postcodes were sorted by north to south

    assert len(found_stores) == 5

    assert len(store_dict) == 7
