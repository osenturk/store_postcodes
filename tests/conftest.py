""" test configuration """

__author__ = "Ozan Senturk"

import yaml
import pytest


@pytest.fixture
def get_config():
    """
        load the config and this method can be called
        for each test cases as a parameter

    :return: config file object
    """

    config_path = "config.yaml"
    with open(config_path, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    return cfg
