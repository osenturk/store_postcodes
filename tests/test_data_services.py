"""
    testing data services
"""

__author__ = "Ozan Senturk"

import json
from services.data_services import import_stores
from services.data_services import load_config

def test_get_config(get_config):
    """
        tests to load the config file

    :param get_config:
    :return:
    """

    file_path = get_config["store"]["file_path"]
    assert file_path == "data/stores.json"


def test_json_load(get_config):
    """
        tests to load the JSON file into the memory.

    :param get_config:
    :return:
    """

    file_path = get_config["store"]["file_path"]

    with open(file_path) as file:
        data = json.load(file)

    assert data[0]["name"] == "St_Albans"
    assert data[0]["postcode"] == "AL1 2RJ"

    assert data[-1]["name"] == "Borehamwood"
    assert data[-1]["postcode"] == "WD6 4PR"


def test_import_stores(get_config):
    """
        tests the test data by converting JSON file
        into the dictionary.

    :param get_config:
    :return:
    """

    file_path = get_config["store"]["file_path"]
    store_dict = import_stores(file_path)

    store = store_dict["AL1 2RJ"]

    assert store.name == "St_Albans"


def test_load_config():
    """
        tests the load_config method of the data services module
    :return:
    """

    cfg = load_config()

    assert cfg["postcode_io"]["radius"] == "2000m"
