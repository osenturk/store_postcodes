"""
There are 2 service end points

/api/get_stores
/api/get_store_lats

"""
__author__ = "Ozan Senturk"


import logging

from flask import Flask, render_template

# custom packages for the project
from services.data_services import load_config
from services.data_services import import_stores
from services.postcode_services import get_bulk_latlong_by_postcodes

app = Flask(__name__)

logger = logging.getLogger(__name__)


@app.route("/api/get_stores", methods=["GET"])
def get_stores():
    """
        renders the stores

    :return:
    """

    cfg = load_config()
    store_dict = import_stores(
        cfg["store"]["file_path"]
    )  # stores were cached in the dictionary

    store_keys = list(store_dict)

    store_list = [store_dict[key] for key in store_keys]

    logger.info("the store list was loaded")

    store_list.sort(key=lambda x: x.name, reverse=False)  # sort by name

    logger.info("the store list was sorted by name")

    return render_template("stores.html", store_list=store_list)


@app.route("/api/get_store_lats", methods=["GET"])
def get_store_lats():
    """
        renders the stores within latitudes and longitudes

    :return:
    """

    cfg = load_config()  # refresh the config

    # limit per request for postcode.id
    request_limit = cfg["postcode_io"]["bulk_postcode_limit"]

    if request_limit > 100:
        logger.warning(
            "The API Bulk Postcode Lookup limit is 100 "
            "your bulk_postcode_limit is set to 100 "
            "whilst it was %d",
            request_limit,
        )
        request_limit = 100

    logger.info("the request limit is %d", request_limit)

    store_dict = import_stores(
        cfg["store"]["file_path"]
    )  # stores were cached in the dictionary

    store_dict = get_bulk_latlong_by_postcodes(store_dict, request_limit)

    logger.info("the latitudes and longitudes were retrieved")

    store_keys = list(store_dict)

    store_list = [store_dict[key] for key in store_keys]

    store_list_with_lats = list(filter(lambda x: x.lat is not None,
                                       store_list))

    store_list_with_lats.sort(key=lambda x: x.lat,
                              reverse=True)  # sort by lat

    return render_template("stores.html", store_list=store_list_with_lats)


@app.route("/", methods=["GET"])
def index():
    """
        renders the home page

    :return:
    """

    return render_template("index.html")


if __name__ == "__main__":

    import logging.config

    logging.config.fileConfig("logging.conf")

    app.run(debug=True, host="0.0.0.0", port=4000)
